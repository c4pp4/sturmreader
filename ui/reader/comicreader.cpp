 /* Copyright 2015 Robert Schroll
  * Copyright 2021 Emanuele Sorce emanuele.sorce@hotmail.com
 * 
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */

#include "comicreader.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QtGui/QImage>
#include <QBuffer>
#include <QDir>
#include <QString>
#include <QCryptographicHash>
#include <QDebug>
#include <quazip.h>
#include <quazipfile.h>
#include "../qhttpserver/qhttpresponse.h"

QString guessMimeType(const QString &filename);

ComicReader::ComicReader(QObject *parent) :
    QObject(parent)
{
    zip = NULL;
}

bool ComicReader::load(const QString &filename)
{
    if (zip != NULL) {
        delete zip;
        zip = NULL;
    }
    _hash = "";
    spine.clear();

    zip = new QuaZip(filename);
    if (!zip->open(QuaZip::mdUnzip)) {
        delete zip;
        zip = NULL;
        return false;
    }
    if (!parse()) {
        delete zip;
        zip = NULL;
        return false;
    }
    return true;
}

int ComicReader::numberOfPages() {
	if(!zip) {
		qDebug() << "Comic file not open";
		return 0;
	}
	return spine.length();
}

bool ComicReader::parse() {
    QList<QuaZipFileInfo> fileList = zip->getFileInfoList();
	// config files and metadata - ignored but don't fail
	// See https://wiki.mobileread.com/wiki/CBR_and_CBZ
	QStringList ignored_extensions = {
		".xml", ".config", ".json", ".acbf"
	};
    
	foreach (const QuaZipFileInfo info, fileList) {
		if (info.uncompressedSize > 0) {
			bool valid = true;
			for(int e=0; e<ignored_extensions.length(); e++) {
				if(info.name.endsWith(ignored_extensions[e], Qt::CaseInsensitive)) {
					valid = false;
					break;
				}
			}
			if(valid)
				spine.append(info.name);
		}
    }
    return true;
}

QString ComicReader::hash() {
    if (_hash != "")
        return _hash;

    if (!zip || !zip->isOpen())
        return _hash;

    QByteArray CRCarray;
    QDataStream CRCstream(&CRCarray, QIODevice::WriteOnly);
    QList<QuaZipFileInfo> fileList = zip->getFileInfoList();
    foreach (const QuaZipFileInfo info, fileList) {
        CRCstream << info.crc;
    }
    _hash = QCryptographicHash::hash(CRCarray, QCryptographicHash::Md5).toHex();
    return _hash;
}

QString ComicReader::title() {
    return "";
}

void ComicReader::serveComponent(int pagenum, QHttpResponse *response)
{
    if (!zip || !zip->isOpen()) {
        response->writeHead(500);
        response->end("Epub file not open for reading");
        return;
    }
	// we start from 0 for the first page
	pagenum = pagenum - 1;

	if( pagenum >= numberOfPages() ) {
		response->writeHead(404);
		response->end(QString("Out of range page request: " + pagenum).toUtf8());
		return;
	}
    
    zip->setCurrentFile(spine[pagenum]);
    QuaZipFile zfile(zip);
    if (!zfile.open(QIODevice::ReadOnly)) {
        response->writeHead(404);
        response->end(QString("Could not find \"" + spine[pagenum] + "\" in epub file").toUtf8());
        return;
    }

    response->setHeader("Content-Type", guessMimeType(spine[pagenum]));
    response->writeHead(200);
    // Important -- use write instead of end, so binary data doesn't get messed up!
    response->write(zfile.readAll());
    response->end();
    zfile.close();
}

QVariantList ComicReader::getContents()
{
    QVariantList res;
//     for (int i=0; i<spine.length(); i++) {
//         QVariantMap entry;
//         entry["title"] = "%PAGE% " + QString::number(i + 1);
//         entry["src"] = spine[i];
//         res.append(entry);
//     }
    emit contentsReady(res);
    return res;
}

QVariantMap ComicReader::getCoverInfo(int thumbsize, int fullsize)
{
    QVariantMap res;
    if (!zip || !zip->isOpen())
        return res;

    res["title"] = "ZZZnone";
    res["author"] = "";
    res["authorsort"] = "zzznone";
    res["cover"] = "ZZZnone";

    zip->setCurrentFile(spine[0]);
    QuaZipFile zfile(zip);
    if (!zfile.open(QIODevice::ReadOnly))
        return res;

    QImage coverimg;
    if (!coverimg.loadFromData(zfile.readAll())) {
        zfile.close();
        return res;
    }
    zfile.close();
    QByteArray byteArray;
    QBuffer buffer(&byteArray);
    coverimg.scaledToWidth(thumbsize, Qt::SmoothTransformation).save(&buffer, "PNG");
    res["cover"] = "data:image/png;base64," + QString(byteArray.toBase64());
    QByteArray byteArrayf;
    QBuffer bufferf(&byteArrayf);
    coverimg.scaledToWidth(fullsize, Qt::SmoothTransformation).save(&bufferf, "PNG");
    res["fullcover"] = "data:image/png;base64," + QString(byteArrayf.toBase64());
    return res;
}
