// current page
var pageNumber = 1;
// Aspet ratio of the book currently loaded
var book_aspect_ratio = 1;
// true until the first render is performed
var first_render = true;
// hammer manager
var mc = undefined;
// total number of pages in the book
var number_of_pages = 999999;
// if a page turning is already ongoing, and we ignore other turns
var ignore_page_turning = true;

// BOOK PAGE API

function statusUpdate() {
	messaging.send("pageNumber " + pageNumber);
	messaging.send("UpdatePage");
}
function moveToLocus(locus) {
	if(ignore_page_turning) return;
	moveToPage(locus.pageNumber, false);
}
var styleManager = {
	updateStyles: function(style) {
		if(style.pdfBackground)
			document.body.style.background = style.pdfBackground;
		
		if(!first_render)
			messaging.send("ok");
	}
}
function setNumberOfPages(n) {
	number_of_pages = n;
	if(pageNumber > number_of_pages) {
		console.log("WARNING! Page number too big, redirecting to last page");
		pageNumber = Math.min(number_of_pages, pageNumber);
		moveTopage(pageNumber, true);
	}
}

// END BOOK PAGE CALLS

// force_and_silent: trigger render even if it's already current page, and don't send "Jumping" messages
function moveToPage(target, force_and_silent) {
	// Don't trigger turning if it's the same page
	// In theory ignore_page_turning and force_and_silent should never conflict
	if((target != pageNumber && !ignore_page_turning) || force_and_silent) {
		ignore_page_turning = true;
		
		let delta = target - pageNumber;
		
		// update page number
		pageNumber = target;
		
		if(delta == 1 || delta == -1) {
			move_image.onload = function() {
				// set move_page over main one
				move_image.style.visibility = "visible";
				move_image.classList.add("transitionPageOut");
				
				main_image.onload = function() {
					// move image
					move_image.style.left = delta == 1 ? "-100%" : "+100%";
					afterRendering();
				}
				renderPage(pageNumber).then( data => {
					main_image.src = data
				})
			}
			// moving page will display old page
			move_image.src = main_image.src;
		}
		// bigger jump - no animation - reset cache
		else {
			renderPage(pageNumber).then( data => {
				main_image.onload = function() {
					afterRendering().then(
						() => { ignore_page_turning = false; }
					)
				}
				main_image.src = data;
			})
		}
	}
	// Send response to stop loading anyway
	else {
		if(target == pageNumber && !ignore_page_turning)
			messaging.send("ok");
		ignore_page_turning = false;
	}
}

async function afterRendering() {
	// finalize stuff
	book_aspect_ratio = main_image.width / main_image.height;
	centerImages();
	
	// Communicate with QML
	if(first_render) {
		first_render = false;
		messaging.send("Ready");
	}
	messaging.send("status_requested");
}

function centerImages() {
	if(window.innerWidth / window.innerHeight < book_aspect_ratio) {
		main_image.style.top = "50%";
		main_image.style.transform = "translateY(-50%)";
		move_image.style.top = "50%";
		move_image.style.transform = "translateY(-50%)";
	} else {
		main_image.style.top = "0px";
		main_image.style.transform = "translateY(-0px)";
		move_image.style.top = "0px";
		move_image.style.transform = "translateY(-0px)";
	}
}

const readFileAsText = (inputBlob) => {
	const temporaryFileReader = new FileReader();
	return new Promise((resolve, reject) => {
		temporaryFileReader.onerror = () => {
			temporaryFileReader.abort();
			reject(new DOMException("Problem parsing input file."));
		};
		temporaryFileReader.onload = () => {
			resolve(temporaryFileReader.result);
		};
		temporaryFileReader.readAsDataURL(inputBlob);
	});
};

async function renderPage(target_page) {
	var response = await fetch("http://127.0.0.1:" + messaging.port + "/" + target_page);
	var blob = await response.blob();
	var image = await readFileAsText(blob);
	return image;
}

function tapPageTurn(ev) {
	if(ignore_page_turning) return;
	
	// do not move if zoomed
	if(window.visualViewport.scale > 1.001)
		return;
	
	if(ev.center.x > window.innerWidth * 0.4) {
		if(pageNumber < number_of_pages)
			moveToPage(pageNumber + 1);
	} else {
		if(pageNumber > 1)
			moveToPage(pageNumber - 1);
	}
}

function transitionPageTurned() {
	move_image.classList.remove("transitionPageOut");
	move_image.style.visibility = "hidden";
	move_image.style.left = "0px";
	ignore_page_turning = false;
}

window.onload = function() {
	messaging.send("request_number_of_pages");
	
	// initalize images
	main_image = document.getElementById('main');
	main_image.onerror = function() { messaging.send("# page rendering failed: page main - reason: onerror"); }
	move_image = document.getElementById('move');
	move_image.onerror = function() { messaging.send("# page rendering failed: page move - reason: onerror"); }
	
	move_image.style.visibility = "hidden";
	
	// initialize gestures
	mc = new Hammer.Manager(document.getElementById("container"));
	var Tap = new Hammer.Tap();
	var Press = new Hammer.Press();
	// Add the recognizer to the manager
	mc.add(Press);
	mc.add(Tap);
	mc.on("tap press", (ev) => tapPageTurn(ev) );
	
	// set saved styles
	if(DEFAULT_STYLES)
		styleManager.updateStyles(DEFAULT_STYLES);
	
	// load saved page or open from the beginning
	if(SAVED_PLACE && SAVED_PLACE.pageNumber && Number(SAVED_PLACE.pageNumber) > 0)
		pageNumber = SAVED_PLACE.pageNumber;
	
	// initialize event listeners
	move_image.addEventListener("transitionend", transitionPageTurned);
	
	moveToPage(pageNumber, true);
}

past_width = window.innerWidth;
past_height = window.innerHeight;
window.onresize = function() {
	centerImages();
	
	// trigger a background rendering //if resolution increase
 	if(window.innerWidth > past_width || window.innerHeight > past_height)
		moveToPage(pageNumber, true);
	
	var past_width = window.innerWidth;
 	var past_height = window.innerHeight;
}
