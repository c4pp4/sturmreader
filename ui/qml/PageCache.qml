/* Copyright 2020 Emanuele Sorce
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL v3. See the file COPYING for full details.
 */

import QtQuick 2.12

QtObject {
	id: pagecache
	
	property alias pageNumber
	property alias reader
	// API
	
	// get page, if not present in the cache, render it and return it
	function get( num ) {}
	
}
